package com.sk.laboratorio2compose.exercices.rolldice

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel
import com.sk.laboratorio2compose.R

class DiceRollerViewModel : ViewModel() {
    var diceResult by mutableStateOf(R.drawable.dice_1)

    fun rollDice() {
        val randomDice = (1..6).random()
        val drawableResource = when (randomDice) {
            1 -> R.drawable.dice_1
            2 -> R.drawable.dice_2
            3 -> R.drawable.dice_3
            4 -> R.drawable.dice_4
            5 -> R.drawable.dice_5
            else -> R.drawable.dice_6
        }
        diceResult = drawableResource
    }
}