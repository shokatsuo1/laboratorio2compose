package com.sk.laboratorio2compose.exercices

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.Switch
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.sk.laboratorio2compose.ui.theme.ChineseWhite

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun TipCalculator() {
    var checked by remember { mutableStateOf(false) }
    var billAmount by remember { mutableStateOf(0.0) }
    var tipPercentage by remember { mutableStateOf(0) }
    val tipAmount = (tipPercentage / 100.0) * billAmount
    val roundedTipAmount = if (checked) kotlin.math.ceil(tipAmount) else tipAmount
    Box(
        modifier = Modifier
            .fillMaxSize(),
        contentAlignment = Alignment.TopCenter
    ) {
        Column(
            modifier = Modifier.padding(32.dp),
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            Text(
                text = "Calculate Tip",
                textAlign = TextAlign.Center,
                fontSize = 28.sp,
                fontWeight = FontWeight.Normal
            )

            Spacer(modifier = Modifier.height(32.dp))

            OutlinedTextField(
                value = billAmount.toString(),
                onValueChange = { newValue ->
                    billAmount = newValue.toDoubleOrNull() ?: 0.0
                },
                placeholder = {
                    Text("Bill amount")
                },
                keyboardOptions = KeyboardOptions.Default.copy(
                    keyboardType = KeyboardType.Number
                ),
                modifier = Modifier
                    .fillMaxWidth()
                    .background(ChineseWhite)
            )

            Spacer(modifier = Modifier.height(16.dp))


            OutlinedTextField(
                value = tipPercentage.toString(),
                onValueChange = { newValue ->
                    tipPercentage = newValue.toIntOrNull() ?: 0
                },
                placeholder = {
                    Text("Tip (%)")
                },
                keyboardOptions = KeyboardOptions.Default.copy(
                    keyboardType = KeyboardType.Number
                ),
                modifier = Modifier
                    .fillMaxWidth()
                    .background(ChineseWhite)
            )

            Spacer(modifier = Modifier.height(16.dp))

            Row(
                verticalAlignment = Alignment.CenterVertically,
                horizontalArrangement = Arrangement.SpaceBetween,
                modifier = Modifier
                    .fillMaxWidth()
            ) {
                Text(
                    text = "Round up tip?",
                    fontSize = 18.sp
                )

                Switch(
                    checked = checked,
                    onCheckedChange = { isChecked ->
                        checked = isChecked
                    }
                )
            }
            Spacer(modifier = Modifier.height(32.dp))
            Text(
                text = "Tip Amount: $$roundedTipAmount",
                fontSize = 22.sp,
                fontWeight = FontWeight.Bold
            )
        }
    }
}

@Preview(name = "Tip Calculator", showSystemUi = true)
@Composable
fun TipCalculatorPreview(){
    TipCalculator()
}